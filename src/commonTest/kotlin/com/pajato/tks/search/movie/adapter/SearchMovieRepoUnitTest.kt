package com.pajato.tks.search.movie.adapter

import com.pajato.tks.common.adapter.TmdbApiService.TMDB_BASE_API3_URL
import com.pajato.tks.common.adapter.TmdbFetcher
import com.pajato.tks.common.core.InjectError
import com.pajato.tks.common.core.SearchKey
import com.pajato.tks.common.core.jsonFormat
import com.pajato.tks.pager.core.PageData
import com.pajato.tks.pager.core.PagedResultMovie
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlin.test.fail

class SearchMovieRepoUnitTest {
    private val apiKey = "8G9B0WX31T2U7P0Q"
    private val urlConverterMap: MutableMap<String, String> = mutableMapOf()
    private val key: SearchKey = SearchKey("Movie", "abc", 1)
    private val url = "$TMDB_BASE_API3_URL/search/movie?api_key=$apiKey&query=abc&page=1"

    private lateinit var errorMessage: String
    private lateinit var errorExc: String
    private lateinit var errorExcMessage: String
    private var isFetched: Boolean = false

    @BeforeTest fun setUp() {
        TmdbFetcher.inject(::fetch, ::handleError)
        TmdbFetcher.inject(apiKey)
        errorMessage = ""
        errorExc = ""
        errorExcMessage = ""
        urlConverterMap.clear()
        isFetched = false
    }

    private fun fetch(url: String): String {
        isFetched = true
        val name = urlConverterMap[url] ?: fail("Key $url is not mapped to a test resource name!")
        return getJson(name).ifEmpty { throw IllegalStateException("No JSON available!") }
    }

    private fun getJson(name: String): String = javaClass.classLoader.getResource(name)?.readText() ?: ""

    private fun handleError(message: String, exc: Exception?) {
        errorMessage = message
        errorExc = if (exc == null) "null" else exc.javaClass.name
        errorExcMessage = if (exc == null) "null" else exc.message ?: "null"
    }

    @Test fun `When accessing a cached search page, verify cache access and no fetch is performed`() {
        val resourceName = "movie.json"
        val searchPage: PageData<PagedResultMovie> = jsonFormat.decodeFromString(getJson(resourceName))
        TmdbSearchMovieRepo.cache[key] = searchPage
        runBlocking {
            val actualPage = TmdbSearchMovieRepo.getSearchPageMovie(key)
            assertEquals(searchPage, actualPage)
            assertFalse(isFetched)
        }
    }

    @Test fun `When accessing a cached search page, verify the results`() {
        val resourceName = "movie.json"
        val searchPage: PageData<PagedResultMovie> = jsonFormat.decodeFromString(getJson(resourceName))
        TmdbSearchMovieRepo.cache[key] = searchPage
        runBlocking {
            val actualResults = TmdbSearchMovieRepo.getSearchResults(key)
            assertEquals(1, actualResults.size)
            assertFalse(isFetched)
        }
    }

    @Test fun `When accessing a non-cached search page, verify search page is fetched from TMDb`() {
        val resourceName = "movie.json"
        val searchPage: PageData<PagedResultMovie> = jsonFormat.decodeFromString(getJson(resourceName))

        urlConverterMap[url] = resourceName
        runBlocking {
            assertEquals(searchPage, TmdbSearchMovieRepo.getSearchPageMovie(key))
            assertTrue(isFetched)
        }
    }

    @Test fun `When the repo is reset, verify inject error behavior`() {
        val key = SearchKey("Movie", "xyzzy", 0)
        TmdbFetcher.reset()
        runBlocking { assertFailsWith<InjectError> { TmdbSearchMovieRepo.getSearchPageMovie(key) } }
    }
}
