package com.pajato.tks.search.movie.adapter

import com.pajato.tks.common.adapter.Strategy
import com.pajato.tks.common.adapter.TmdbFetcher.fetch
import com.pajato.tks.common.core.SearchKey
import com.pajato.tks.pager.core.PageData
import com.pajato.tks.pager.core.PageData.Companion.serializer
import com.pajato.tks.pager.core.PagedResultMovie
import com.pajato.tks.search.movie.core.SearchRepoMovie

/**
 * A repository implementation for searching movies using the TMDB (The Movie Database) API.
 *
 * This object handles caching of search results to reduce unnecessary network calls and
 * ensures efficient data retrieval. It implements the `SearchRepoMovie` interface, which
 * outlines the required functionalities for a search repository that handles movies.
 *
 * @constructor Creates an empty TMDB search movie repository.
 */
public object TmdbSearchMovieRepo : SearchRepoMovie {
    internal val cache: MutableMap<SearchKey, PageData<PagedResultMovie>> = mutableMapOf()

    /**
     * Retrieves a page of movie search results based on the given search key.
     *
     * @param key The search key containing the query and page information.
     * @return A SearchPage object containing a list of search result movies.
     */
    override suspend fun getSearchPageMovie(key: SearchKey): PageData<PagedResultMovie> {
        val path = "search/movie"
        val extraParams = listOf("query=${key.query}", "page=${key.page}")
        val serializer: Strategy<PageData<PagedResultMovie>> = serializer(PagedResultMovie.serializer())
        val defaultPage = PageData<PagedResultMovie>(0, listOf(), 0, 0)
        return cache[key] ?: fetch(path, extraParams, key, serializer, defaultPage).also { cache[key] = it }
    }

    /**
     * Retrieves a list of search result movies based on the given search key.
     *
     * @param key The search key containing query parameters.
     * @return A list of PagedResultMovie objects.
     */
    override suspend fun getSearchResults(key: SearchKey): List<PagedResultMovie> = getSearchPageMovie(key).results
}
